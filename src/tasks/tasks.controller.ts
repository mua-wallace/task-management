import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { CreateTaskDto } from './dtos/create-task.dto';
import { GetTaskFilterDto } from './dtos/get-task-filter.dto';
import { UpdateTaskStatus } from './dtos/update-task-status.dto';
import { Task } from './task.entity';
import { TasksService } from './tasks.service';

@Controller('tasks')
export class TasksController {
    constructor(private tasksService: TasksService) { }


    @Get()
    getTasks(@Query() filterDto: GetTaskFilterDto): Promise<Task[]> {
        return this.tasksService.getTasks(filterDto)
    }




    @Get('/:id')
    getTaskById(@Param('id') id: string): Promise<Task>  {
        return this.tasksService.getTaskById(id)
    }

    @Post()
    createTask(@Body() createTaskDto: CreateTaskDto): Promise<Task>  {
        return this.tasksService.createTask(createTaskDto)

    }


    @Delete('/:id')
    deleteTask(@Param('id') id: string): Promise<void> {
      return this.tasksService.deleteTask(id);
    }


    @Patch(':id/status')
    updateTaskStatus(@Param('id') id: string, @Body() updateTaskStatus: UpdateTaskStatus): Promise<Task> {

        const {status} = updateTaskStatus;
        return this.tasksService.updateTaskStatus(id, status);
    }


}
